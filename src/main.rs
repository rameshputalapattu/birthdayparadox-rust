extern crate rand;
extern crate threadpool;

use rand::Rng;
use std::thread;
use std::sync::mpsc::channel;
use threadpool::ThreadPool;
use std::time;




fn assign_birthdays(num_people:i32,num_common:i32) -> i32 {
     let mut birthdates = [0i32;366];

     for _i in 0..num_people {
         let birthday = rand::thread_rng().gen_range(0,366);
         birthdates[birthday] += 1;
     }

    for i in birthdates.iter() {
        if *i >= num_common {
            return 1
        }
    }
    return 0

}

fn simulate(num_trials:i32,num_people:i32,num_common:i32) -> (i32,f64) {
    let mut num_hits = 0;
    for _ in 0..num_trials {
        num_hits += assign_birthdays(num_people,num_common)
    }
    return (num_people,num_hits as f64/num_trials as f64);
}

fn main() {

    let start = time::SystemTime::now();

    let num_people_arr = vec![1,2,5,10,20,23,30,40,50,60,70,75,80,90,100,366];

    let num_trials = 10000;



    const NTHREADS:usize = 8;

    let executors = ThreadPool::new(NTHREADS);

    let (tx,rx) = channel();

    for num_people in num_people_arr.clone() {
            let tx = tx.clone();
        executors.execute(move || {
           tx.send(simulate(num_trials,num_people,2)).unwrap() ;
        });

    }




    for (num_people,prob) in rx.iter().take(num_people_arr.len()) {

        //let (num_people,prob) = rx.recv();
        println!("probability for atleast 2 among {} people sharing their birthday is {:#?}",
                 num_people,prob);

    }



    executors.join();
    let end = time::SystemTime::now();
    let diff = end.duration_since(start).unwrap();
    println!("completed the executions in {} sec",diff.as_secs());




}
